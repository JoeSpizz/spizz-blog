---
title: "Create Pipeline"
date: 2024-02-22
draft: false
headingPost: "Author: Chad Feeser"
---
To use GitLab CI/CD to generate your static site pages with Hugo and serve them via GitLab Pages, you'll need to set up a `.gitlab-ci.yml` file in your repository. This file tells GitLab how to build your site using Hugo within a Docker container. Here's how to do it:

### Step 1: Create a `.gitlab-ci.yml` File

1. **In the root of your GitLab repository**, create a new file named `.gitlab-ci.yml`.
2. Add the following configuration to the file:

```yaml
image: klakegg/hugo:0.101.0-ext # Use an image with Hugo installed. Adjust the version as needed.

variables:
  GIT_SUBMODULE_STRATEGY: recursive # Ensures Hugo themes or other submodules are fetched

pages:
  script:
    - hugo # This command builds your site
  artifacts:
    paths:
      - public # This is where Hugo generates your site by default
  only:
    - master # This job runs only on the master branch (or main, adjust accordingly)
```

This configuration does the following:

- **`image`**: Specifies a Docker image that includes Hugo. The `klakegg/hugo` image is a popular choice. You can adjust the tag to match the version of Hugo you want to use. The `-ext` variant includes extended SCSS/SASS support.
- **`variables`**: Sets the `GIT_SUBMODULE_STRATEGY` to `recursive`, which is important if your Hugo site uses themes or other content added as Git submodules.
- **`pages`**: Defines the job that GitLab CI/CD will execute to build your site.
  - **`script`**: The command that builds your site using Hugo.
  - **`artifacts`**: Specifies the directory that Hugo generates your site into (`public` by default). GitLab Pages uses this directory to serve your site.
  - **`only`**: Ensures the job runs only on changes to the `master` branch (or `main`, depending on your setup).

### Step 2: Push `.gitlab-ci.yml` to Your Repository

After setting up your `.gitlab-ci.yml` file, commit and push it to your repository:

```bash
git add .gitlab-ci.yml
git commit -m "Add GitLab CI/CD configuration for Hugo"
git push origin master
```

### Step 3: Verify the CI/CD Pipeline

Once you push the `.gitlab-ci.yml` file, GitLab CI/CD will automatically trigger a pipeline to build your Hugo site:

1. **Go to the "CI/CD" > "Pipelines" section** of your GitLab repository to view the running pipeline.
2. Once the pipeline completes, it will deploy your static site to GitLab Pages.

### Step 4: Accessing Your Site

- After the pipeline successfully completes, your site should be available on GitLab Pages.
- You can find the URL of your site in the **"Settings" > "Pages"** section of your GitLab repository.

### Additional Notes

- If your Hugo site uses a custom theme added as a submodule, ensure that the theme's repository is also accessible to GitLab CI/CD.
- If you use a branch other than `master` (e.g., `main`), adjust the `only` section in the `.gitlab-ci.yml` file accordingly.
- You can customize the Hugo build command (`hugo`) with additional flags as needed, such as `hugo --minify` for minifying the output.

By following these steps, you've configured GitLab CI/CD to automatically build and deploy your Hugo site to GitLab Pages whenever changes are pushed to your repository.
